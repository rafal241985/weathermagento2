<?php


namespace Monogo\Weather\Setup;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table_monogo_weather_weather = $setup->getConnection()->newTable($setup->getTable('monogo_weather_weather'));

        
        $table_monogo_weather_weather->addColumn(
            'weather_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true],
            'Entity ID'
        );
        

        
        $table_monogo_weather_weather->addColumn(
            'weather',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Weather Data'
        );
        

        
        $table_monogo_weather_weather->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        );
        

        $setup->getConnection()->createTable($table_monogo_weather_weather);

        $setup->endSetup();
    }
}
