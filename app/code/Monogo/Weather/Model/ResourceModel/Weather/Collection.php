<?php


namespace Monogo\Weather\Model\ResourceModel\Weather;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Monogo\Weather\Model\Weather',
            'Monogo\Weather\Model\ResourceModel\Weather'
        );
    }
}
