<?php


namespace Monogo\Weather\Model;

/**
 * Class WeatherRepository
 *
 * @package Monogo\Weather\Model
 */
class WeatherRepository
{
    /**
     * @var null
     */
    private $objectManager = null;

    /**
     * @var null
     */
    private $resource = null;

    /**
     * @var null
     */
    private $connection = null;

    /**
     * WeatherRepository constructor.
     */
    public function __construct()
    {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->connection = $this->resource->getConnection();
    }

    /**
     * Get last weather for
     *
     * @return array
     */
    public function getLastWeather()
    {
        $tableName = $this->connection->getTableName('monogo_weather_weather');
        $query = $this->connection->select()
            ->from($tableName)
            ->order('weather_id DESC')
            ->limit(1);

        $result = $this->connection->fetchRow($query);

        return $result;
    }
}
