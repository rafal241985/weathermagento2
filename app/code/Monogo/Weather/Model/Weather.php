<?php


namespace Monogo\Weather\Model;

use Monogo\Weather\Api\Data\WeatherInterface;

class Weather extends \Magento\Framework\Model\AbstractModel implements WeatherInterface
{

    protected $_eventPrefix = 'monogo_weather_weather';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Monogo\Weather\Model\ResourceModel\Weather');
    }

    /**
     * Get weather_id
     * @return string
     */
    public function getWeatherId()
    {
        return $this->getData(self::WEATHER_ID);
    }

    /**
     * Set weather_id
     * @param string $weatherId
     * @return \Monogo\Weather\Api\Data\WeatherInterface
     */
    public function setWeatherId($weatherId)
    {
        return $this->setData(self::WEATHER_ID, $weatherId);
    }

    /**
     * Get weather
     * @return string
     */
    public function getWeather()
    {
        return $this->getData(self::WEATHER);
    }

    /**
     * Set weather
     * @param string $weather
     * @return \Monogo\Weather\Api\Data\WeatherInterface
     */
    public function setWeather($weather)
    {
        return $this->setData(self::WEATHER, $weather);
    }

    /**
     * Get created_at
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Monogo\Weather\Api\Data\WeatherInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
