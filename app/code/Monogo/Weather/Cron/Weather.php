<?php

namespace Monogo\Weather\Cron;

use Zend\Http\Request;
use Zend\Http\Client;

class Weather
{
    /**
     * Weather api key
     */
    const API_KEY = '839fa5f8de0bb2bd0ec9a8b35b446932';

    /**
     * City id for weather
     *
     * https://api.openweathermap.org/data/2.5/weather?id=7530819&appid=839fa5f8de0bb2bd0ec9a8b35b446932
     */
    const CITY_ID = 765876;

    /**
     * Weather endpoint
     */
    const WEATHER_ENDPOINT = 'https://api.openweathermap.org/data/2.5/weather?id=%s&appid=%s&units=metric';

    /**
     * Logger
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $url = sprintf(self::WEATHER_ENDPOINT, self::CITY_ID, self::API_KEY);

        $request = new Request();
        $request->setUri($url);
        $request->setMethod('GET');

        $client = new Client();
        $options = [
            'adapter'   => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
            'maxredirects' => 0,
            'timeout' => 30
        ];
        $client->setOptions($options);

        $response = $client->send($request);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $weatherObject = $objectManager->create('\Monogo\Weather\Model\Weather');
        $weatherObject->setWeather($response->getBody());
        $weatherObject->save();
    }
}
