<?php


namespace Monogo\Weather\Api\Data;

interface WeatherSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Weather list.
     * @return \Monogo\Weather\Api\Data\WeatherInterface[]
     */
    public function getItems();

    /**
     * Set weather list.
     * @param \Monogo\Weather\Api\Data\WeatherInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
