<?php


namespace Monogo\Weather\Api\Data;

interface WeatherInterface
{

    const CREATED_AT = 'created_at';
    const WEATHER = 'weather';
    const WEATHER_ID = 'weather_id';


    /**
     * Get weather_id
     * @return string|null
     */
    public function getWeatherId();

    /**
     * Set weather_id
     * @param string $weatherId
     * @return \Monogo\Weather\Api\Data\WeatherInterface
     */
    public function setWeatherId($weatherId);

    /**
     * Get weather
     * @return string|null
     */
    public function getWeather();

    /**
     * Set weather
     * @param string $weather
     * @return \Monogo\Weather\Api\Data\WeatherInterface
     */
    public function setWeather($weather);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Monogo\Weather\Api\Data\WeatherInterface
     */
    public function setCreatedAt($createdAt);
}
