<?php


namespace Monogo\Weather\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface WeatherRepositoryInterface
{


    /**
     * Save Weather
     * @param \Monogo\Weather\Api\Data\WeatherInterface $weather
     * @return \Monogo\Weather\Api\Data\WeatherInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Monogo\Weather\Api\Data\WeatherInterface $weather
    );

    /**
     * Retrieve Weather
     * @param string $weatherId
     * @return \Monogo\Weather\Api\Data\WeatherInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($weatherId);

    /**
     * Retrieve Weather matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Monogo\Weather\Api\Data\WeatherSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Weather
     * @param \Monogo\Weather\Api\Data\WeatherInterface $weather
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Monogo\Weather\Api\Data\WeatherInterface $weather
    );

    /**
     * Delete Weather by ID
     * @param string $weatherId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($weatherId);
}
