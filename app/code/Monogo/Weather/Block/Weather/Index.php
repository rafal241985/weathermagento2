<?php


namespace Monogo\Weather\Block\Weather;

class Index extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Get last updated weather
     *
     * @return mixed
     */
    public function getWether()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $weatherObject = $objectManager->create('\Monogo\Weather\Model\WeatherRepository');

        $weather = $weatherObject->getLastWeather();

        return $weather;
    }
}
